# MultitechConduit-mCardMTAC-libloragw-AU915

**Introduction**
The MultiConnect Conduit is a configurable, scalable cellular communications gateway for industrial IoT applications. Conduit allows users to plug in two MultiConnect mCard accessory cards supporting wired or wireless interfaces.

**MultiTech Conduit**
<blockquote class="imgur-embed-pub" lang="en" data-id="a/WixgXYv"><a href="//imgur.com/WixgXYv"></a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>
This guide will help you set up the gateway to communicate over The Things Network.


**Prerequisites**
MultiTech Conduit [AEP](http://www.multitech.net/developer/software/aep/)
or [mLinux model](http://www.multitech.net/developer/software/mlinux/).

There is no need to update any of the MultiTech software on the conduit.

[MultiTech MTAC-LoRa LoRa accessory card](http://www.multitech.net/developer/products/multiconnect-conduit-platform/accessory-cards/mtac-lora/), installed as instructed.

**You need:**

* Phillips screwdriver
* MultiConnect mCard Accessory card
* Gateway device



**To install the accessory card:**


1. Disconnect power to the gateway device. 
WARNING: Failing to disconnect power before installing the accessory card may damage both the gateway and the accessory card.
2. At the back of the housing, determine where you want to install the accessory card. 
3. You can install the card in either the AP1 or AP2 port. 
4. Remove the port cover and retain the screw.
5. Slide the card into the opening and push until you feel the card connector seat in the internal connector.
6. Use a small Phillips screwdriver to attach the card bracket to the housing with the screw from the port cover.

http://www.multitech.net/developer/wp-content/uploads/2015/03/MTCDT-MTAC-LORA-Install.png

Do not forget to mount the antenna to the mCard after fitting it in the conduit.

Computer with USB port and terminal software. Mac OS and Linux come with terminal software. For Windows you can use something like Putty.
For the mLinux version you’ll need a USB stick.